# -*- encoding: utf-8 -*-
import cPickle


def load(fileName):
    data = None
    with open(fileName, "rb") as f:
        data = cPickle.load(f)
    return data


def save(fileName, data):
    with open(fileName, "wb") as f:
        cPickle.dump(data, f, cPickle.HIGHEST_PROTOCOL)