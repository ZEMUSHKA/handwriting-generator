# -*- encoding: utf-8 -*-
import os
import codecs
import json
from collections import defaultdict


letterImageFormat = u"{letter}({caseMask})_{index}.png"


def getCaseMask(string):
    caseString = ""
    for f in [l == l.lower() for l in string]:
        caseString += str(int(f))
    return caseString


def loadLettersDb(lettersDbName):
    if os.path.exists(lettersDbName):
        with codecs.open(lettersDbName, "rb", "utf-8") as f:
            lettersDb = defaultdict(list, json.loads(f.read()))
    else:
        lettersDb = defaultdict(list)
    return lettersDb


def saveLettersDb(lettersDbName, lettersDb):
    with codecs.open(lettersDbName, "wb", "utf-8") as f:
        f.write(json.dumps(lettersDb, ensure_ascii=False))
