# -*- coding: utf-8 -*-
from PyQt4 import QtCore, QtGui, uic
import os
import subprocess
import platform

# workaround, cxFreeze will not have that .ui file
if os.path.exists('MainWindow.ui'):
    with open('ui_mainwindow.py', 'wb') as f:
        uic.compileUi('MainWindow.ui', f)

if os.path.exists('res.qrc'):
    if platform.system() == 'Darwin':
        pyrccPath = "/usr/local/bin/pyrcc4"
    else:
        pyrccPath = "C:\Python27\Lib\site-packages\PyQt4\pyrcc4"
    subprocess.call([pyrccPath, "-o", "res_rc.py", "-py2", "res.qrc"])

from ui_mainwindow import Ui_MainWindow

import Utils


class MainWindow(QtGui.QMainWindow, Ui_MainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)

        self.setupUi(self)

        self.w = 800
        self.h = 600
        self.resize(self.w, self.h)

        self.centralWidget.setLayout(self.centralWidget_layout)
        self.centralWidget.setContentsMargins(6, 6, 6, 6)

        # connect button
        QtCore.QObject.connect(self.openImageButton, QtCore.SIGNAL("clicked()"),
                               self, QtCore.SLOT("openImageClicked()"))
        QtCore.QObject.connect(self.scaleSelector, QtCore.SIGNAL("valueChanged(int)"),
                               self, QtCore.SLOT("scaleChanged(int)"))
        QtCore.QObject.connect(self.canvas, QtCore.SIGNAL("newPreviewSignal(QImage)"),
                               self.clippedPreview, QtCore.SLOT("setImage(QImage)"))
        QtCore.QObject.connect(self.binThresholdBox, QtCore.SIGNAL("valueChanged(int)"),
                               self.clippedPreview, QtCore.SLOT("setNewBinThreshold(int)"))
        QtCore.QObject.connect(self.addLetterButton, QtCore.SIGNAL("clicked()"),
                               self, QtCore.SLOT("addLetterInDb()"))
        QtCore.QObject.connect(self.textEdit, QtCore.SIGNAL("textChanged(QString)"),
                               self.generator, QtCore.SLOT("setText(QString)"))

        self.binThresholdBox.valueChanged.emit(128)

        self.lettersFolder = "letters"
        if not os.path.exists(self.lettersFolder):
            os.mkdir(self.lettersFolder)

        self.letterFile = "letters.json"
        self.lettersDbName = os.path.join(self.lettersFolder, self.letterFile)
        self.lettersDb = Utils.loadLettersDb(self.lettersDbName)
        self.generator.setLettersDbPath(self.lettersFolder, self.letterFile)

        self.clippedPreview.setSliders(self.leftBorderSlider, self.rightBorderSlider,
                                       self.topBorderSlider, self.bottomBorderSlider)

    @QtCore.pyqtSlot()
    def openImageClicked(self):
        dialog = QtGui.QFileDialog()
        dialog.setFileMode(QtGui.QFileDialog.ExistingFile)
        selectedFiles = None
        if dialog.exec_():
            selectedFiles = dialog.selectedFiles()
        if selectedFiles:
            fileName = selectedFiles[0]
            if fileName:
                image = QtGui.QImage(fileName)
                image = image.convertToFormat(QtGui.QImage.Format_RGB16)
                self.canvas.setImage(image)

    @QtCore.pyqtSlot(int)
    def scaleChanged(self, scale):
        self.canvas.setScaleFactor(scale)

    @QtCore.pyqtSlot()
    def addLetterInDb(self):
        letter = unicode(self.letterEdit.text())
        image = self.clippedPreview.image
        if len(self.lettersDb[letter]) == 0:
            imageIndex = 0
        else:
            imageIndex = max(el[0] for el in self.lettersDb[letter]) + 1
        newImagePath = os.path.join(self.lettersFolder,
                                    Utils.letterImageFormat.format(letter=letter,
                                                                   caseMask=Utils.getCaseMask(letter),
                                                                   index=imageIndex))
        image.save(newImagePath)
        self.lettersDb[letter].append((imageIndex, self.clippedPreview.getBounding()))
        Utils.saveLettersDb(self.lettersDbName, self.lettersDb)

    def closeEvent(self, event):
        Utils.saveLettersDb(self.lettersDbName, self.lettersDb)
