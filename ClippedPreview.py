# -*- encoding: utf-8 -*-
from PyQt4 import QtGui, QtCore
import math


class ClippedPreview(QtGui.QWidget):
    def __init__(self, parent=None):
        super(ClippedPreview, self).__init__(parent)
        self.image = None
        self.origImage = None
        self.binThreshold = None

    @QtCore.pyqtSlot(QtGui.QImage)
    def setImage(self, image):
        self.origImage = image
        self.binarizeAndCropImage()
        self.update()

    def binarizeAndCropImage(self):
        # binarize image
        left = 1e20
        right = -1e20
        bottom = -1e20
        top = 1e20

        if not self.origImage:
            return

        image = QtGui.QImage(self.origImage)
        image = image.convertToFormat(QtGui.QImage.Format_ARGB32)
        for i in xrange(image.width()):
            for j in xrange(image.height()):
                oldPixel = QtGui.QColor(image.pixel(i, j)).green()
                newPixel = 0 if oldPixel <= self.binThreshold else 255
                image.setPixel(i, j, QtGui.qRgba(newPixel, newPixel, newPixel, 0 if newPixel == 255 else 255))
                if newPixel == 0:
                    left = min(left, i)
                    right = max(right, i)
                    bottom = max(bottom, j)
                    top = min(top, j)

        # crop to black only
        if 0 <= left < image.width() and 0 <= right < image.width() \
                and 0 <= top < image.height() and 0 <= bottom < image.height():
            image = image.copy(left, top, right - left + 1, bottom - top + 1)
        else:
            image = QtGui.QImage()

        self.image = image

    def getScale(self):
        w, h = self.width(), self.height()
        imH, imW = self.image.height(), self.image.width()
        scale = 1.0 / max(imW / float(w), imH / float(h))
        return scale, imW * scale, imH * scale

    def paintEvent(self, event):
        painter = QtGui.QPainter(self)
        if self.image and self.image.height() > 0 and self.image.width() > 0:
            scale, scaledW, scaledH = self.getScale()
            painter.drawImage(0, 0, self.image.scaled(scaledW, scaledH))

            # paint sliders
            sliderValues = self.slidersToPixels()
            painter.setPen(QtCore.Qt.red)
            for name in ("l", "r"):
                painter.drawLine(sliderValues[name], 0, sliderValues[name], self.height())
            for name in ("t", "b"):
                painter.drawLine(0, sliderValues[name], self.width(), sliderValues[name])

    @QtCore.pyqtSlot(int)
    def setNewBinThreshold(self, thd):
        self.binThreshold = thd
        self.binarizeAndCropImage()
        self.update()

    def setSliders(self, left, right, top, bottom):
        self.sliders = {
            "l": left,
            "r": right,
            "t": top,
            "b": bottom
        }

        # connect sliders to get repaint
        for name, slider in self.sliders.iteritems():
            QtCore.QObject.connect(slider, QtCore.SIGNAL("valueChanged(int)"),
                                   self, QtCore.SLOT("newSliderValueSlot(int)"))

    @QtCore.pyqtSlot(int)
    def newSliderValueSlot(self, ignored):
        self.update()

    def slidersToPixels(self):
        sliderValues = {}

        for name, slider in self.sliders.iteritems():
            value = slider.value()
            if name in ("t", "b"):
                value = slider.maximum() - value
            sliderValues[name] = value / float(slider.maximum())

        scale, scaledW, scaledH = self.getScale()

        for name in ("l", "r"):
            sliderValues[name] *= self.width()
            if sliderValues[name] >= scaledW:
                sliderValues[name] = scaledW - 1
        for name in ("t", "b"):
            sliderValues[name] *= self.height()
            if sliderValues[name] >= scaledH:
                sliderValues[name] = scaledH - 1

        return sliderValues

    def getBounding(self):
        sliderValues = self.slidersToPixels()

        scale, scaledW, scaledH = self.getScale()

        for name in sliderValues.iterkeys():
            sliderValues[name] /= scale
            sliderValues[name] = int(math.floor(sliderValues[name]))

        return sliderValues