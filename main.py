# -*- coding: utf-8 -*-
import sys
from PyQt4 import QtGui, QtCore
import MainWindow


application = QtGui.QApplication(sys.argv)
mainWindow = MainWindow.MainWindow()
mainWindow.show()
sys.exit(application.exec_())
