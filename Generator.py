# -*- encoding: utf-8 -*-
from PyQt4 import QtGui, QtCore
import os
import random

import Utils


class Generator(QtGui.QWidget):
    def __init__(self, parent=None):
        super(Generator, self).__init__(parent)
        self.text = ""

    @QtCore.pyqtSlot(QtCore.QString)
    def setText(self, text):
        text = unicode(text)
        self.text = text
        self.update()

    def setLettersDbPath(self, folder, fileName):
        self.lettersFolder = folder
        self.lettersFileName = os.path.join(folder, fileName)

    def paintEvent(self, event):
        painter = QtGui.QPainter(self)
        lettersDB = Utils.loadLettersDb(self.lettersFileName)

        topLine = int(self.height() * 4 / 10.0)
        bottomLine = int(self.height() * 5 / 10.0)
        leftLine = self.height() / 10
        rightLine = leftLine

        for letter in self.text:
            if letter == " ":
                rightLine += self.height() / 6

            if letter in lettersDB:
                letterImageIndex, bounding = lettersDB[letter][random.randint(0, len(lettersDB[letter]) - 1)]
                imageName = Utils.letterImageFormat.format(letter=letter,
                                                           caseMask=Utils.getCaseMask(letter),
                                                           index=letterImageIndex)
                imagePath = os.path.join(self.lettersFolder, imageName)
                letterImage = QtGui.QImage(imagePath)

                scale = float(bottomLine - topLine) / (bounding["b"] - bounding["t"])
                upperCornerY = topLine - bounding["t"] * scale
                upperCornerX = rightLine - bounding["l"] * scale

                painter.drawImage(upperCornerX, upperCornerY,
                                  letterImage.scaled(letterImage.width() * scale, letterImage.height() * scale,
                                                     transformMode=QtCore.Qt.SmoothTransformation))

                rightLine += (bounding["r"] - bounding["l"]) * scale
