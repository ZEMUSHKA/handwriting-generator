# -*- encoding: utf-8 -*-
from PyQt4 import QtGui, QtCore


class Canvas(QtGui.QWidget):
    newPreviewSignal = QtCore.pyqtSignal(QtGui.QImage)

    def __init__(self, parent=None):
        super(Canvas, self).__init__(parent)
        self.w = 0
        self.h = 0
        self.image = None
        self.scaledImage = None
        self.scale = 1
        self.drawTrace = []
        self.saveClippedFlag = False

    def setImage(self, image):
        self.w = image.width()
        self.h = image.height()
        self.image = image
        self.scaleImage()
        self.update()

    def scaleImage(self):
        if self.image:
            self.scaledW = self.w * self.scale
            self.scaledH = self.h * self.scale
            self.scaledImage = self.image.scaled(self.scaledW, self.scaledH)
            self.setMinimumSize(self.scaledW, self.scaledH)

    def setScaleFactor(self, scale):
        self.scale = scale
        self.drawTrace = []
        self.scaleImage()
        self.update()

    def paintEvent(self, event):
        painter = QtGui.QPainter(self)

        # paint image
        if self.w > 0 and self.h > 0:
            painter.drawImage(0, 0, self.scaledImage)

        # paint cursor path
        if len(self.drawTrace) > 1:
            path = QtGui.QPainterPath()
            startX, startY = self.drawTrace[0]
            path.moveTo(startX, startY)
            for cursor in self.drawTrace[1:]:
                x, y = cursor
                path.lineTo(x, y)
            painter.drawPath(path)

            if self.saveClippedFlag:
                self.saveClipped(path)
                self.saveClippedFlag = False

    def mouseMoveEvent(self, event):
        cursor = (event.x(), event.y())
        self.drawTrace.append(cursor)
        self.update()

    def mousePressEvent(self, event):
        self.drawTrace = []

    def mouseReleaseEvent(self, event):
        self.saveClippedFlag = True
        self.update()

    def saveClipped(self, path):
        # no image
        if not self.scaledImage:
            return

        bounding = path.boundingRect()

        # bad path
        if bounding.width() == 0 or bounding.height() == 0:
            return

        subImage = self.scaledImage.copy(bounding.x(), bounding.y(), bounding.width(), bounding.height())

        # draw subImage with stencil :)
        clippedImage = QtGui.QPixmap(bounding.width(), bounding.height())
        painter = QtGui.QPainter(clippedImage)
        painter.fillRect(clippedImage.rect(), QtCore.Qt.white)
        # translate path bounding box upperLeft to (0, 0)
        translatedPath = path
        translatedPath.translate(-bounding.x(), -bounding.y())
        painter.setClipPath(translatedPath)
        painter.drawImage(0, 0, subImage)
        painter.end()
        clippedImage = clippedImage.scaled(bounding.width() / self.scale, bounding.height() / self.scale)
        clippedImage = clippedImage.toImage()

        # clippedImage.save("clippedImage_debug.png")
        self.newPreviewSignal.emit(clippedImage)
